/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./classwork/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./classwork/index.js":
/*!****************************!*\
  !*** ./classwork/index.js ***!
  \****************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _observer__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./observer */ \"./classwork/observer.js\");\n/* harmony import */ var _player__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./player */ \"./classwork/player.js\");\n// Точка входа в наше приложение\n\n\n//import CustomEvents from './observer/CustomEvents';\n// import obs from '../classworks/observer';\n\n\nObject(_observer__WEBPACK_IMPORTED_MODULE_0__[\"default\"])()\n\n// 0. HOC\n// HOF();\n// 1. Observer ->\n// console.log( Observer );\n// Observer();\n// console.log('INDEX');\n// 2. CustomEvents ->\n//CustomEvents();\n\n\n//# sourceURL=webpack:///./classwork/index.js?");

/***/ }),

/***/ "./classwork/observer.js":
/*!*******************************!*\
  !*** ./classwork/observer.js ***!
  \*******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/*\n  Задание: Модуль создания плейлиста, используя паттерн Обсервер.\n\n  У вас есть данные о исполнителях и песнях. Задание делится на три модуля:\n    1. Список исполнителей и песен (Находится слева) - отуда можно включить\n    песню в исполнение иди добавить в плейлист.\n    Если песня уже есть в плейлисте, дважды добавить её нельзя.\n\n    2. Плейлист (Находится справа) - список выбраных песен, песню можно удалить,\n    или запустить в исполнение. Внизу списка должен выводиться блок, в котором\n    пишет суммарное время проигрывания всех песен в плейлисте.\n\n    3. Отображает песню которая проигрывается.\n\n    4. + Бонус: Сделать прогресс пар того как проигрывается песня\n    с возможностью его остановки.\n*/\n\nconst MusicList = [\n  {\n    title: 'Rammstain',\n    songs: [\n      {\n        id: 1,\n        name: 'Du Hast',\n        time: [3, 12]\n      },\n      {\n        id: 2,\n        name: 'Ich Will',\n        time: [5, 1]\n      },\n      {\n        id: 3,\n        name: 'Mutter',\n        time: [4, 15]\n      },\n      {\n        id: 4,\n        name: 'Ich tu dir weh',\n        time: [5, 13]\n      },\n      {\n        id: 5,\n        name: 'Rammstain',\n        time: [3, 64]\n      }\n    ]\n  },\n  {\n    title: 'System of a Down',\n    songs: [\n      {\n        id: 6,\n        name: 'Toxicity',\n        time: [4, 22]\n      },\n      {\n        id: 7,\n        name: 'Sugar',\n        time: [2, 44]\n      },\n      {\n        id: 8,\n        name: 'Lonely Day',\n        time: [3, 19]\n      },\n      {\n        id: 9,\n        name: 'Lost in Hollywood',\n        time: [5, 9]\n      },\n      {\n        id: 10,\n        name: 'Chop Suey!',\n        time: [2, 57]\n      }\n    ]\n  },\n  {\n    title: 'Green Day',\n    songs: [\n      {\n        id: 11,\n        name: '21 Guns',\n        time: [4, 16]\n      },\n      {\n        id: 12,\n        name: 'Boulevard of broken dreams!',\n        time: [6, 37]\n      },\n      {\n        id: 13,\n        name: 'Basket Case!',\n        time: [3, 21]\n      },\n      {\n        id: 14,\n        name: 'Know Your Enemy',\n        time: [4, 11]\n      }\n    ]\n  },\n  {\n    title: 'Linkin Park',\n    songs: [\n      {\n        id: 15,\n        name: 'Numb',\n        time: [3, 11]\n      },\n      {\n        id: 16,\n        name: 'New Divide',\n        time: [4, 41]\n      },\n      {\n        id: 17,\n        name: 'Breaking the Habit',\n        time: [4, 1]\n      },\n      {\n        id: 18,\n        name: 'Faint',\n        time: [3, 29]\n      }\n    ]\n  }\n]\n\nconst MusicBox = () => {\n  const MusicBox = document.getElementById('MusicBox');\n  MusicList.map( Artist => {\n    MusicBox.innerHTML += `<div><h4>${Artist.title}</h4><ul>`;\n    Artist.songs.map( song => {\n      MusicBox.innerHTML += `<li>${song.name}</li>`;\n    })\n    MusicBox.innerHTML += '</ul></div>';\n  });\n}\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (MusicBox);\n\n\n//# sourceURL=webpack:///./classwork/observer.js?");

/***/ }),

/***/ "./classwork/player.js":
/*!*****************************!*\
  !*** ./classwork/player.js ***!
  \*****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/*\n  Задание: Модуль создания плейлиста, используя паттерн Обсервер.\n\n  У вас есть данные о исполнителях и песнях. Задание делится на три модуля:\n    1. Список исполнителей и песен (Находится слева) - отуда можно включить\n    песню в исполнение иди добавить в плейлист.\n    Если песня уже есть в плейлисте, дважды добавить её нельзя.\n\n    2. Плейлист (Находится справа) - список выбраных песен, песню можно удалить,\n    или запустить в исполнение. Внизу списка должен выводиться блок, в котором\n    пишет суммарное время проигрывания всех песен в плейлисте.\n\n    3. Отображает песню которая проигрывается.\n\n    4. + Бонус: Сделать прогресс пар того как проигрывается песня\n    с возможностью его остановки.\n*/\n\nconst MusicList = [\n  {\n    title: 'Rammstain',\n    songs: [\n      {\n        id: 1,\n        name: 'Du Hast',\n        time: [3, 12]\n      },\n      {\n        id: 2,\n        name: 'Ich Will',\n        time: [5, 1]\n      },\n      {\n        id: 3,\n        name: 'Mutter',\n        time: [4, 15]\n      },\n      {\n        id: 4,\n        name: 'Ich tu dir weh',\n        time: [5, 13]\n      },\n      {\n        id: 5,\n        name: 'Rammstain',\n        time: [3, 64]\n      }\n    ]\n  },\n  {\n    title: 'System of a Down',\n    songs: [\n      {\n        id: 6,\n        name: 'Toxicity',\n        time: [4, 22]\n      },\n      {\n        id: 7,\n        name: 'Sugar',\n        time: [2, 44]\n      },\n      {\n        id: 8,\n        name: 'Lonely Day',\n        time: [3, 19]\n      },\n      {\n        id: 9,\n        name: 'Lost in Hollywood',\n        time: [5, 9]\n      },\n      {\n        id: 10,\n        name: 'Chop Suey!',\n        time: [2, 57]\n      }\n    ]\n  },\n  {\n    title: 'Green Day',\n    songs: [\n      {\n        id: 11,\n        name: '21 Guns',\n        time: [4, 16]\n      },\n      {\n        id: 12,\n        name: 'Boulevard of broken dreams!',\n        time: [6, 37]\n      },\n      {\n        id: 13,\n        name: 'Basket Case!',\n        time: [3, 21]\n      },\n      {\n        id: 14,\n        name: 'Know Your Enemy',\n        time: [4, 11]\n      }\n    ]\n  },\n  {\n    title: 'Linkin Park',\n    songs: [\n      {\n        id: 15,\n        name: 'Numb',\n        time: [3, 11]\n      },\n      {\n        id: 16,\n        name: 'New Divide',\n        time: [4, 41]\n      },\n      {\n        id: 17,\n        name: 'Breaking the Habit',\n        time: [4, 1]\n      },\n      {\n        id: 18,\n        name: 'Faint',\n        time: [3, 29]\n      }\n    ]\n  }\n]\n\nconst MusicBox = () => {\n  const MusicBox = document.getElementById('MusicBox');\n  MusicList.map( Artist => {\n    MusicBox.innerHTML += `<div><h4>${Artist.title}</h4><ul>`;\n    Artist.songs.map( song => {\n      MusicBox.innerHTML += `<li>${song.name}</li>`;\n    })\n    MusicBox.innerHTML += '</ul></div>';\n  });\n}\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (MusicBox);\n\n\n//# sourceURL=webpack:///./classwork/player.js?");

/***/ })

/******/ });